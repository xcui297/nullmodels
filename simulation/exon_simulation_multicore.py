import pandas as pd
import numpy as np
import sys
#from sympy.utilities.iterables import multiset_permutations
import itertools
import time
import multiprocessing as mp

def get_exon_len(exon_set,plen):
    len_set = [exon_set[0]]
    num_exon = len(exon_set)
    for i in range(1,num_exon):
        len_set.append(exon_set[i]-exon_set[i-1])
    len_set.append(plen-exon_set[-1])
    return len_set

def convert_len_to_pos(exon_len_set):
    num_exon = len(exon_len_set)
    exon_pos_set = [np.sum(exon_len_set[:i+1]) for i in range(num_exon-1)]
    return exon_pos_set

def measure_O(exon_boundary_pos,domain_box_pos):
    O = 0
    for exon in exon_boundary_pos:
        if exon != int(exon):
            exon_l = exon-0.5
            exon_r = exon+0.5
        else:
            exon_l = exon
            exon_r = -1
        for box in domain_box_pos:
            box_start = box[0]
            box_end = box[1]
            if (((exon_l >= box_start) and (exon_l <= box_end)) or ((exon_r >= box_start) and (exon_r <= box_end))):
                O += 1
                break
    return O


exon_file = sys.argv[1]
domain_file = sys.argv[2]
protein_len_file = sys.argv[3]
out_file = sys.argv[4]
numcore = int(sys.argv[5])
exon_df = pd.read_csv(exon_file,delimiter='\t',header=None).values
domain_df = pd.read_csv(domain_file,delimiter='\t',header=None).values
domain_tids = domain_df[:,0]
boxes = {}

for i in range(len(domain_tids)):
    tid = domain_tids[i]
    if not (tid in boxes):
        boxes[tid] = []
    boxes[tid].append((domain_df[i,1],domain_df[i,2]))


protein_len_df = pd.read_csv(protein_len_file,delimiter='\t',header=None).values
protein_len = {}
for i in range(protein_len_df.shape[0]):
    protein_len[protein_len_df[i,0]] = protein_len_df[i,1]

exons = {}
for i in range(exon_df.shape[0]):
    tid = exon_df[i,0]
    exon_info = exon_df[i,1]
    if '-' in exon_info:
        exon = int(exon_info.split('-')[0])+0.5
    else:
        exon = int(exon_info)
    if not (tid in exons):
        exons[tid] = []
    exons[tid].append(exon)

exon_len = {}
j = 0
nump = len(exons)


def permute_one_gene(tid):
    exons[tid] = np.sort(exons[tid])
    exon_len[tid] = get_exon_len(exons[tid],protein_len[tid])
    tmp = exon_len[tid]
    Os = []
    if len(tmp) > 9:
        starttime = time.time()
        for i in range(362880):
            cur_exon_len = np.random.permutation(tmp)
            exon_boundary_pos = convert_len_to_pos(cur_exon_len)
            O = measure_O(exon_boundary_pos,boxes[tid])
            Os.append(O)
            tmp = cur_exon_len
        endtime = time.time()
        elapsed = endtime-starttime
        perm_mes = "%s exon number %d, incomplete permutation, took me %.2f" % (tid,len(tmp),elapsed)
    else:
        perm_set = list(itertools.permutations(np.arange(0,len(tmp),1)))
        perm_mes = "%s perm set size %d" % (tid,len(perm_set))
        for perm in perm_set:
            idx = np.array(perm).astype(int)
            cur_exon_len = [tmp[i] for i in idx]
            exon_boundary_pos = convert_len_to_pos(cur_exon_len)
            O = measure_O(exon_boundary_pos,boxes[tid])
            Os.append(O)
    return [tid, Os, perm_mes]


pool = mp.Pool(processes=numcore)
#results = [pool.apply_async(permute_one_gene,args=(tid,)) for tid in exons]
counter = 0
with open(out_file,'w') as f:
    for result in pool.imap_unordered(permute_one_gene,exons):
        counter += 1
        tid = result[0]
        Os = result[1]
        perm_mes = result[2]
        perm_mes = "(%d/%d)" % (counter,nump) + perm_mes
        print(perm_mes)
        out_mes = tid
        for O in Os:
            out_mes = out_mes +'\t'+str(O)
        out_mes = out_mes+'\n'
        f.write(out_mes)
pool.close()
pool.join()
