import sys
import pandas as pd
import numpy as np
import time
import multiprocessing as mp

samplesize = 10000

species = sys.argv[1]
plenin = "../domain_simulation_20/"+species+"_domain/protein_length.txt"
exonpos = "../domain_simulation_20/"+species+"_domain/exon_boundary_for_exon_simulation.txt"
pinter = "../domain_simulation_20/"+species+"_domain/protein_intersection.txt"
domain_file = "../boundary_20/"+species+"_window20/domain_box_for_exon_simulation.txt"

domain_df = pd.read_csv(domain_file,delimiter='\t',header=None).values
domain_tids = domain_df[:,0]
boxes = {}

for i in range(len(domain_tids)):
    tid = domain_tids[i]
    if not (tid in boxes):
        boxes[tid] = []
    boxes[tid].append((domain_df[i,1],domain_df[i,2]))

proteins_used = np.loadtxt(pinter,dtype=str,comments=None)

plendata = pd.read_csv(plenin,delimiter='\t',header=None).values
plens = {}
for i in range(plendata.shape[0]):
    tid = plendata[i,0]
    if tid in proteins_used:
        plens[tid] = plendata[i,1]


def get_exon_len(exon_set,plen):
    len_set = [exon_set[0]]
    num_exon = len(exon_set)
    for i in range(1,num_exon):
        len_set.append(exon_set[i]-exon_set[i-1])
    len_set.append(plen-exon_set[-1])
    return len_set

exon_df = pd.read_csv(exonpos,delimiter='\t',header=None).values

nexon = {}
exons = {}
for i in range(exon_df.shape[0]):
    tid = exon_df[i,0]
    if tid in proteins_used:
        exon_info = exon_df[i,1]
        if '-' in exon_info:
            exon = int(exon_info.split('-')[0])+0.5
        else:
            exon = int(exon_info)
        if not (tid in exons):
            exons[tid] = []
            nexon[tid] = 1
        exons[tid].append(exon)
        nexon[tid] += 1

allens = []
for tid in exons:
    exons[tid] = np.sort(exons[tid])
    exon_len = get_exon_len(exons[tid],plens[tid])
    allens = [*allens,*exon_len]

allens = np.array(allens)
allens = allens[allens>0]
allO = np.zeros((len(exons),samplesize))

def sample_good_exon(myn,targetlen):
    strial = 0
    pexons = np.random.choice(allens,myn)
    sumlen = np.sum(pexons)
    diff = np.abs(targetlen-sumlen)
    strial += 1
    while ((diff > 100) and (myn < 20)):
        if strial > 20:
            break
        strial += 1
        pexons = np.random.choice(allens,myn)
        sumlen = np.sum(pexons)
        diff = np.abs(targetlen-sumlen)
    if not (sumlen==targetlen):
        factor = targetlen/sumlen
        pexons = [exon*factor for exon in pexons]
    return pexons

def convert_len_to_pos(exon_len_set):
    num_exon = len(exon_len_set)
    exon_pos_set = [np.sum(exon_len_set[:i+1]) for i in range(num_exon-1)]
    return exon_pos_set


def measure_O(exon_boundary_pos,domain_box_pos):
    O = 0
    for exon in exon_boundary_pos:
        if exon != int(exon):
            exon_l = exon-0.5
            exon_r = exon+0.5
        else:
            exon_l = exon
            exon_r = -1
        for box in domain_box_pos:
            box_start = box[0]
            box_end = box[1]
            if (((exon_l >= box_start) and (exon_l <= box_end)) or ((exon_r >= box_start) and (exon_r <= box_end))):
                O += 1
                break
    return O

def adjust_pos(exon_boundary_pos):
    for i in range(len(exon_boundary_pos)):
        exon = exon_boundary_pos[i]
        if (exon < int(exon)+0.17):
            exon_boundary_pos[i] = int(exon)+0.5
        elif (exon < int(exon)+0.83):
            exon_boundary_pos[i] = int(exon)+1
        else:
            exon_boundary_pos[i] = int(exon)+1.5
    return exon_boundary_pos



def sample_one_gene(tid):
    n = nexon[tid]
    Os = np.zeros(samplesize)
    starttime = time.time()
    tidlen = plens[tid]
    tidbox = boxes[tid]
    for i in range(samplesize):
        cur_exon_len = sample_good_exon(n,tidlen)
        exon_boundary_pos = convert_len_to_pos(cur_exon_len)
        exon_boundary_pos = adjust_pos(exon_boundary_pos)
        Os[i] = measure_O(exon_boundary_pos,tidbox)
    endtime = time.time()
    elapsed = endtime-starttime
    perm_mes = "%s exon number %d, took me %.2f" % (tid,n,elapsed)
    return [tid, Os, perm_mes]

numcore = 50
pool = mp.Pool(processes=numcore)
counter = 0
nump = len(exons)
totalO = 0
for result in pool.imap_unordered(sample_one_gene,exons):
    tid = result[0]
    Os = np.array(result[1])
    perm_mes = result[2]
    allO[counter,:] = Os
    totalO += np.mean(Os)
    counter += 1
    perm_mes = "(%d/%d)" % (counter,nump) + perm_mes
    print(perm_mes)
pool.close()
pool.join()
np.save(species+"_lognormals.npy",allO)
print(totalO)
