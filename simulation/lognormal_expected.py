import numpy as np
import sys

# perm_file = sys.argv[1]
# f = open(perm_file)
# line = f.readline()
# total = 0
# while line:
#     info = line.split('\t')
#     tmp = info[1:]
#     O = [int(float(O)) for O in tmp]
#     total += np.mean(O)
#     line = f.readline()
# f.close()
# print(total)

lognormals = np.load(sys.argv[1])
means = np.mean(lognormals,axis=1)
print(np.sum(means))
