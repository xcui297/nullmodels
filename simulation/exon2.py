import pandas as pd
import numpy as np
import sys
#from sympy.utilities.iterables import multiset_permutations
import itertools
import time
import multiprocessing as mp
#ITER = 100000000
iter = int(sys.argv[3])
numcore = int(sys.argv[4])
O_hash = {}
perm_file = sys.argv[1]
f = open(perm_file)
n_gene = 21116

def sample_one_gene(xixi):
    line = f.readline()
    if not line:
        return np.zeros(iter)
    info = line.split('\t')
    tid = info[0]
    tmp = info[1:]
    try:
        data = [int(O) for O in tmp if O!='']
    except Exception as e:
        return np.zeros(iter)
    return np.random.choice(data,iter,replace=True)

n_gene = int(sys.argv[2])
gene_count = 0
Os = np.zeros(iter)
pool = mp.Pool(processes=numcore)
counter = 0
for result in pool.imap_unordered(sample_one_gene,range(n_gene)):
    counter +=1
    if counter % 10 ==0 :
        print("%d/%d" % (counter,n_gene))
    Os += result
pool.close()
pool.join()
np.save('Os.npy',Os)
