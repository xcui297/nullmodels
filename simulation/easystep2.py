import numpy as np
import sys
import multiprocessing as mp

species = sys.argv[1]

lognormals = np.load(species+"_lognormals.npy")
n = lognormals.shape[0]
Os = np.zeros(100000000)

def sample_one_gene(idx):
    return np.random.choice(lognormals[idx,:],100000000)


pool = mp.Pool(processes=50)
counter = 0
for result in pool.imap_unordered(sample_one_gene,np.arange(n)):
    counter += 1
    if counter%10==0:
        print("%d/%d" % (counter,n))
    Os += result
pool.close()
pool.join()
np.save(species+"easyOs.npy",Os)
print(np.mean(Os))
with open("lognormal_step2.txt",'a') as f:
    f.write("%s\t%.2f\n" % (species,np.mean(Os)))
