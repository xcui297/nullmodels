import pandas as pd
import numpy as np
import sys
import itertools
import time
import multiprocessing as mp

exon_file = sys.argv[1]
domain_file = sys.argv[2]
protein_used_file = sys.argv[3]
protein_len_file = sys.argv[4]
out_file = sys.argv[5]
numcore = int(sys.argv[6])

proteins_used = np.loadtxt(protein_used_file,dtype='str',comments=None)

exon_df = pd.read_csv(exon_file,delimiter='\t',header=None).values
exons = {}
for i in range(exon_df.shape[0]):
    tid = exon_df[i,0]
    exon_info = exon_df[i,1]
    if '-' in exon_info:
        exon = int(exon_info.split('-')[0])+0.5
    else:
        exon = int(exon_info)
    if not (tid in exons):
        exons[tid] = []
    exons[tid].append(exon)


domain_df = pd.read_csv(domain_file,delimiter='\t',header=None).values

protein_len_df = pd.read_csv(protein_len_file,delimiter='\t',header=None).values
protein_len = {}
for i in range(protein_len_df.shape[0]):
    protein_len[protein_len_df[i,0]] = protein_len_df[i,1]

def convert_len_to_pos(exon_len_set):
    num_exon = len(exon_len_set)
    exon_pos_set = [np.sum(exon_len_set[:i+1]) for i in range(num_exon-1)]
    return exon_pos_set

def measure_O(exon_boundary_pos,domain_box_pos):
    O = 0
    for single_domain in domain_box_pos:
        agreement = 0
        for one_box in single_domain:
            found_one = False
            box_start = one_box[0]
            box_end = one_box[1]
            for exon in exon_boundary_pos:
                if exon != int(exon):
                    exon_l = exon-0.5
                    exon_r = exon+0.5
                else:
                    exon_l = exon
                    exon_r = -1
                if (((exon_l >= box_start) and (exon_l <= box_end)) or ((exon_r >= box_start) and (exon_r <= box_end))):
                    agreement += 1
                    found_one = True
                    break
            if found_one:
                continue
            else:
                break
        if agreement == 2:
            O += 1
    return O

boxes = {}

for i in range(domain_df.shape[0]):
    tid = domain_df[i,0]
    if tid in proteins_used:
        if not (tid in boxes):
            boxes[tid] = []
        domain_start_box = domain_df[i,2].split('-')
        domain_end_box = domain_df[i,3].split('-')
        domain_start_box = [int(domain_start_box[0]),int(domain_start_box[1])]
        domain_end_box = [int(domain_end_box[0]),int(domain_end_box[1])]
        boxes[tid].append([domain_start_box,domain_end_box])


def get_exon_len(exon_set,plen):
    len_set = [exon_set[0]]
    num_exon = len(exon_set)
    for i in range(1,num_exon):
        len_set.append(exon_set[i]-exon_set[i-1])
    len_set.append(plen-exon_set[-1])
    return len_set


exon_len = {}
j = 0
nump = len(exons)

def permute_one_gene(tid):
    exons[tid] = np.sort(exons[tid])
    exon_len[tid] = get_exon_len(exons[tid],protein_len[tid])
    tmp = exon_len[tid]
    Os = []
    if len(tmp) > 9:
        starttime = time.time()
        for i in range(362880):
            cur_exon_len = np.random.permutation(tmp)
            exon_boundary_pos = convert_len_to_pos(cur_exon_len)
            O = measure_O(exon_boundary_pos,boxes[tid])
            Os.append(O)
        endtime = time.time()
        elapsed = endtime-starttime
        perm_mes = "%s exon number %d, incomplete permutation, took me %.2f" % (tid,len(tmp),elapsed)
    else:
        perm_set = list(itertools.permutations(np.arange(0,len(tmp),1)))
        perm_mes = "%s perm set size %d" % (tid,len(perm_set))
        for perm in perm_set:
            idx = np.array(perm).astype(int)
            cur_exon_len = [tmp[i] for i in idx]
            exon_boundary_pos = convert_len_to_pos(cur_exon_len)
            O = measure_O(exon_boundary_pos,boxes[tid])
            Os.append(O)
    return [tid, Os, perm_mes]

pool = mp.Pool(processes=numcore)


#results = [pool.apply_async(permute_one_gene,args=(tid,)) for tid in exons]
counter = 0
with open(out_file,'w') as f:
    for result in pool.imap_unordered(permute_one_gene,exons):
        counter += 1
        tid = result[0]
        Os = result[1]
        perm_mes = result[2]
        perm_mes =  "(%d/%d)" % (counter,nump) + perm_mes
        print(perm_mes)
        f.write("%s" % tid)
        for O in Os:
            f.write("\t%d" % O)
        f.write("\n")
pool.close()
pool.join()
