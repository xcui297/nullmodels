import pandas as pd
import numpy as np
import sys
#from sympy.utilities.iterables import multiset_permutations
import itertools
import time
import multiprocessing as mp
#ITER = 100000000
iter = int(sys.argv[2])
numcore = int(sys.argv[3])
O_hash = {}
perm_file = sys.argv[1]
f = open(perm_file)
line = f.readline()
mcount = 0
while line:
    mcount += 1
    info = line.split('\t')
    tid = info[0]
    tmp = info[1:]
    O_hash[tid] = [int(float(O)) for O in tmp]
    line = f.readline()
f.close()
print(mcount)
n_gene = len(O_hash)

gene_count = 0
Os = np.zeros(iter)


def sample_one_gene(tid):
    return np.random.choice(O_hash[tid],iter,replace=True)



pool = mp.Pool(processes=numcore)
counter = 0
for result in pool.imap_unordered(sample_one_gene,O_hash):
    counter += 1
    if counter % 10 ==0 :
        print("%d/%d" % (counter,n_gene))
    Os += result
pool.close()
pool.join()


print("and mean is")
print(np.mean(Os))

np.save('Os.npy',Os)


O = 0
for tid in O_hash:
    O += np.mean(O_hash[tid])
print(O)
