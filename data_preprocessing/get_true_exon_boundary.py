import pandas as pd
import numpy as np
import sys




gtf_file = sys.argv[1]
opt = sys.argv[3]

#gtf_file = 'aspergillus_nidulans_used_proteins.gtf'
gtf_df = pd.read_csv(gtf_file,delimiter='\t',header=None)
cds_table = gtf_df.values[gtf_df.values[:,2]=='CDS',:]
tids_info = cds_table[:,8]
if opt =="4":
    tids = [tid_entry.split("\"")[-4] for tid_entry in tids_info]
elif opt == "0":
    tids = tids_info
elif opt =="2":
    tids = [tid_entry.split("\"")[-2] for tid_entry in tids_info]
cds_hash = {}
cds_strand = {}
for i in range(cds_table.shape[0]):
    tid = tids[i]
    cds = [cds_table[i,3],cds_table[i,4]]
    strand = cds_table[i,6]
    cds_strand[tid] = strand
    if tid in cds_hash:
        cds_hash[tid].append(cds)
    else:
        cds_hash[tid] = [cds]
aa_hash = {}



for tid in cds_hash:
    cdss = cds_hash[tid]
    aa = 0
    aa_hash[tid] = {}
    position = 1
    for cds in cdss:
        cds_start = cds[0]
        cds_end = cds[1]
        if cds_strand[tid] == '+':
            for base in range(cds_start,cds_end+1):
                if position == 1:
                    aa += 1
                aa_hash[tid][base] = aa
                position = (position+1)%3
        else:
            for base in range(cds_end,cds_start-1,-1):
                if position == 1:
                    aa += 1
                aa_hash[tid][base] = aa
                position = (position+1)%3


exon_table = gtf_df.values[gtf_df.values[:,2]=='exon',:]
tids_info = exon_table[:,8]
if opt =="4":
    tids = [tid_entry.split("\"")[-4] for tid_entry in tids_info]
elif opt == "0":
    tids = tids_info
elif opt =="2":
    tids = [tid_entry.split("\"")[-2] for tid_entry in tids_info]


selected_exon_file = sys.argv[2]
with open(selected_exon_file,"w") as f:
    for i in range(exon_table.shape[0]):
        tid = tids[i]
        strand = exon_table[i,6]
        if tid in aa_hash:
            exon_l = exon_table[i,3]
            exon_r = exon_table[i,4]
            if strand == '+':
                if exon_l in aa_hash[tid]:
                    exon_pos = aa_hash[tid][exon_l]
                    f.write("%s\t%d\t%s\n" % (tid,exon_pos,"l"))
                if exon_r in aa_hash[tid]:
                    exon_pos = aa_hash[tid][exon_r]
                    f.write("%s\t%d\t%s\n" % (tid,exon_pos,"r"))
            else:
                if exon_r in aa_hash[tid]:
                    exon_pos = aa_hash[tid][exon_r]
                    f.write("%s\t%d\t%s\n" % (tid,exon_pos,"l"))
                    #if tid =="SPPG_08272T0":
                    #    print("exon_r:"+exon_r)
                    #    print("l")

                if exon_l in aa_hash[tid]:
                    exon_pos = aa_hash[tid][exon_l]
                    f.write("%s\t%d\t%s\n" % (tid,exon_pos,"r"))
                    #if tid =="SPPG_08272T0":
                    #    print("exon_l:"+exon_l)
                    #    print("r")
