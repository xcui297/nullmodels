import pandas as pd
import numpy as np
import sys

window_size = 20

domain_file = sys.argv[1]
len_file = sys.argv[2]

protein_len_df = pd.read_csv(len_file,delimiter='\t',header=None).values
protein_len = {}
for i in range(protein_len_df.shape[0]):
    protein_len[protein_len_df[i,0]] = protein_len_df[i,1]

out_file = sys.argv[3]
domain_df = pd.read_csv(domain_file,delimiter='\t',header=None).values
with open(out_file, "w") as f:
    for i in range(domain_df.shape[0]):
        tid = domain_df[i,1]
        if not (tid in protein_len):
            continue
        region = domain_df[i,3].split('-')
        supfamid = domain_df[i,5]
        plen = protein_len[tid]
        start = int(region[0])
        end = int(region[-1])
        if start <= window_size:
            continue
        if end <= window_size:
            continue
        if end > plen - window_size:
            continue
        if start > plen -window_size:
            continue
        start_box_start = start - 10
        start_box_end = start +9
        end_box_start = end -9
        end_box_end = end + 10
        if start_box_start < 0:
            start_box_start = 0
        if end_box_end < 0:
            end_box_end = 0
        f.write("%s\t%d\t%d-%d\t%d-%d\n" % (tid, supfamid,start_box_start,start_box_end,end_box_start,end_box_end))
