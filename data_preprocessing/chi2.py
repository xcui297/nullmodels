import numpy as np
import pandas as pd
import sys
from scipy.stats import chi2
datafile = sys.argv[1]
data = pd.read_csv(datafile,delimiter=',',skiprows=[0],header=None).values
#print(data)
Os = data[:,5]
#print(Os)
E_Smithers = data[:,6]
E_lius = data[:,7]
n_exons = data[:,3]
#print(E_lius)

chi_lius = (Os-E_lius)**2/E_lius
E_liu2 = n_exons-E_lius
O2 = n_exons-Os
chi_lius += (O2-E_liu2)**2/E_liu2

p_lius = [1-chi2.cdf(chi,1) for chi in chi_lius]


chi_Smithers = (Os-E_Smithers)**2/E_Smithers
E_Simithers2 = n_exons-E_Smithers
chi_Smithers += (O2-E_Simithers2)**2/E_Simithers2
p_Smithers = [1-chi2.cdf(chi,1) for chi in chi_Smithers]


E_perm = data[:,8]
chi_perm = (Os-E_perm)**2/E_perm
E_perm2 = n_exons-E_perm
chi_perm += (O2-E_perm2)**2/E_perm2
p_perm = [1-chi2.cdf(chi,1) for chi in chi_perm]


for p in chi_perm:
    print(p)
