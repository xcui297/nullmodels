import pandas as pd
import numpy as np
import sys

def merge_box(a):
    b = []
    for begin,end in sorted(a):
        if b and b[-1][1] >= begin - 1:
            b[-1] = (b[-1][0], end)
        else:
            b.append((begin, end))
    return b

def sum_tid(a):
    return np.sum([end-begin+1 for begin,end in a])


protein_len_file = sys.argv[1]
protein_used_file = sys.argv[2]
domain_box_file = sys.argv[3]
domain_box_out_for_E_exon = sys.argv[4]


protein_len_df = pd.read_csv(protein_len_file,delimiter='\t',header=None).values
protein_len = {}
for i in range(protein_len_df.shape[0]):
    protein_len[protein_len_df[i,0]] = protein_len_df[i,1]

proteins_used = np.loadtxt(protein_used_file,dtype='str',comments=None)

total_len = np.sum([protein_len[tid] for tid in proteins_used])

domain_df = pd.read_csv(domain_box_file,delimiter='\t',header=None).values
boxes = {}
domain_tids = domain_df[:,0]
for i in range(domain_df.shape[0]):
    tid = domain_tids[i]
    if tid in proteins_used:
        if not (tid in boxes):
            boxes[tid] = []
        domain_start_box = domain_df[i,2].split('-')
        domain_end_box = domain_df[i,3].split('-')
        boxes[tid].append((int(domain_start_box[0]),int(domain_start_box[1])))
        boxes[tid].append((int(domain_end_box[0]),int(domain_end_box[1])))

box_len = 0
domain_num = 0
for tid in boxes:
    domain_num += len(boxes[tid])
    boxes[tid] = merge_box(boxes[tid])
    box_len += sum_tid(boxes[tid])
#print(total_len)
#print(box_len)
myfrac = float(box_len)/float(total_len)
#print(float(box_len)/float(total_len))
print("liu frac: %.5f" % myfrac)
#print(domain_num)
for tid in boxes:
    for box in boxes[tid]:
        with open(domain_box_out_for_E_exon, "a") as f:
            f.write("%s\t%d\t%d\n" % (tid, box[0], box[1]))
