import pandas as pd
import sys
import numpy as np

infile = sys.argv[1]
df = pd.read_csv(infile,header=None,comment='#',delimiter='\t').values
sources = []
for i in range(df.shape[0]):
    info = df[i,8].split(';')
    for entry in info:
        if "gene_name" in entry:
            entry = entry.split("\"")[1]
            sources.append(entry)
            break
sources = np.unique(sources)
print(len(sources))
