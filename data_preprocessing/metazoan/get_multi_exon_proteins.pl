#!/usr/bin/perl -w
use strict;

my $transcript_gtf = $ARGV[0];
open(GTF, $transcript_gtf);
my @GTF = <GTF>;
my %transcripts;
my %transcripts_entry;
foreach (@GTF) {
  my @info = split /\t/, $_;
  my $type = $info[2];
  my @id_info = split /;/, $info[8];
  my $tid = $id_info[0];
  if ($type eq 'exon') {
    if (exists $transcripts{$tid}) {
      $transcripts{$tid} += 1;
    } else {
      $transcripts{$tid} = 1;
    }
  }
  push @{$transcripts_entry{$tid}}, $_;
}
close GTF;

my $output_file_name = $ARGV[1];
#used_proteins.gtf
open(OUTPUT, ">$output_file_name");
foreach my $transcript (keys %transcripts) {
  if ($transcripts{$transcript} > 1) {
    foreach (@{$transcripts_entry{$transcript}}) {
      print OUTPUT $_;
    }
  }
}

close OUTPUT;
