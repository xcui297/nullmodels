import pandas as pd
import sys

input_file = sys.argv[1]
df = pd.read_csv(input_file,header=None,comment='#',delimiter='\t').values
outfile = sys.argv[2]
mapfile = sys.argv[3]
maps = {}

with open(outfile,'w') as f:
    for i in range(df.shape[0]):
        #print(df[i,8])
        data = df[i,:]
        data = [str(point) for point in data]
        info = data[8].split(';')
        type = data[2]
        if type == 'exon':
            for entry in info:
                entry_s = entry
                if 'transcript_id' in entry:
                    f.write('\t'.join(data[:8])+'\t'+entry.split('\"')[1]+'\n')
                    break
        elif type =='CDS':
            for entry in info:
                entry_s = entry
                if 'protein_id' in entry:
                    protein_id = entry.split('\"')[1]
                elif 'transcript_id' in entry:
                    transcript = entry.split('\"')[1]
            f.write('\t'.join(data[:8])+'\t'+transcript+'\n')
            maps[transcript] = protein_id
with open(mapfile,'w') as f:
    for transcript in maps:
        protein_id = maps[transcript]
        f.write(transcript+"\t"+protein_id+'\n')
