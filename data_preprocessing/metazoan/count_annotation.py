import sys
import pandas as pd
import numpy as np
infile = sys.argv[1]

df = pd.read_csv(infile,delimiter='\t',header=None).values
proteins = np.unique(df[:,1])
uniqueprotein = []
for p in proteins:
    if not (p in uniqueprotein):
        uniqueprotein.append(p)
print(proteins.shape)
print(len(uniqueprotein))
