import pandas as pd
import sys

input_file = sys.argv[1]
map_file = sys.argv[2]
outfile = sys.argv[3]

df = pd.read_csv(input_file,header=None,comment='#',delimiter='\t').values
mapdata = pd.read_csv(map_file,header=None,delimiter='\t').values
maps = {}
mapsback = {}
exclude_transcript = []
history = {}
for i in range(mapdata.shape[0]):
    transcript = mapdata[i,0]
    protein = mapdata[i,1]
    if not (protein in maps):
        maps[protein] = 'notpossible'
    if not protein in history:
        history[protein] = []
    if protein in maps:
        if maps[protein] != transcript:
            if not (transcript in history[protein]):
                if maps[protein] != 'notpossible':
                    exclude_transcript.append(maps[protein])
                    history[protein].append(maps[protein])
                maps[protein] = transcript
    mapsback[transcript] = protein

selected_transcript = []
for protein in maps:
    selected_transcript.append(maps[protein])

with open(outfile,'w') as f:
    for i in range(df.shape[0]):
        transcript = df[i,8]
        if transcript in mapsback:
            if not (transcript in exclude_transcript):
                data = df[i,:]
                data = [str(point) for point in data]
                protein = mapsback[transcript]
                f.write('\t'.join(data[:8])+'\t'+protein+'\n')
