import numpy as np
import sys

infile = sys.argv[1]
outfile = sys.argv[2]
f = open(infile,'r')
line = f.readline()
plen = 0
lens = {}
while line:
    line = str(line).strip('\n')
    if '>' in line:
        if plen != 0:
            lens[tid]= plen
        tid = line.split(' ')[0].strip('>')
        plen = 0
    else:
        plen += len(line)
    line = f.readline()
lens[tid] = plen
with open(outfile,'w') as f:
    for tid in lens:
        f.write("%s\t%d\n" % (tid,lens[tid]))
