import sys

infile = sys.argv[1]
f = open(infile,'r')
line = f.readline()
count = {}
while line:
    line = str(line).strip('\n')
    if '>' in line:
        info = line.split(' ')
        protein = info[0].strip('>')
        if not (protein in count):
            count[protein] = 1
        else:
            count[protein] += 1
    line = f.readline()
for protein in count:
    if count[protein] > 1:
        print(protein)
