import pandas as pd
import numpy as np
import sys

def merge_box(a):
    b = []
    for begin,end in sorted(a):
        if b and b[-1][1] >= begin - 1:
            b[-1] = (b[-1][0], end)
        else:
            b.append((begin, end))
    return b

def sum_tid(a):
    return np.sum([end-begin+1 for begin,end in a])


protein_len_file = sys.argv[1]
protein_used_file = sys.argv[2]
domain_box_file = sys.argv[3]
exon_boundary_file = sys.argv[4]
exon_out_file = sys.argv[5]

protein_len_df = pd.read_csv(protein_len_file,delimiter='\t',header=None).values
protein_len = {}
for i in range(protein_len_df.shape[0]):
    protein_len[protein_len_df[i,0]] = protein_len_df[i,1]

proteins_used = np.loadtxt(protein_used_file,dtype='str',comments=None)

total_len = np.sum([protein_len[tid] for tid in proteins_used])

domain_df = pd.read_csv(domain_box_file,delimiter='\t',header=None).values

# construct box hash
boxes = {}
domain_tids = domain_df[:,0]
for i in range(domain_df.shape[0]):
    tid = domain_tids[i]
    if tid in proteins_used:
        if not (tid in boxes):
            boxes[tid] = []
        domain_start_box = domain_df[i,2].split('-')
        domain_end_box = domain_df[i,3].split('-')
        boxes[tid].append((int(domain_start_box[0]),int(domain_start_box[1])))
        boxes[tid].append((int(domain_end_box[0]),int(domain_end_box[1])))

exon_df = pd.read_csv(exon_boundary_file,delimiter='\t',header=None).values
exon = {}
for i in range(exon_df.shape[0]):
    tid = exon_df[i,0]
    if not (tid in exon):
        exon[tid] = 0
    exon[tid] += 1
    if tid in proteins_used:
        with open(exon_out_file, "a") as f:
            f.write("%s\t%s\n" % (tid, exon_df[i,1]))


box_len = {}
domain_num = 0
total_boxlen = 0
for tid in boxes:
    domain_num += len(boxes[tid])
    boxes[tid] = merge_box(boxes[tid])
    box_len[tid] = sum_tid(boxes[tid])
    total_boxlen += box_len[tid]

E = 0
total_proteinlen = 0
total_exon = 0
total_protein = 0
for tid in proteins_used:
    total_protein += 1
    total_proteinlen += protein_len[tid]
    total_exon += exon[tid]
    E += float(exon[tid])*float(box_len[tid])/float(protein_len[tid])
#print(total_boxlen)
#print(total_proteinlen)
#print(total_exon)
print("Smithers E: %.2f" % E)
