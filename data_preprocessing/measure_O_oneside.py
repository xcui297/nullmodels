import pandas as pd
import numpy as np
import sys

exon_file = sys.argv[1]
domain_file = sys.argv[2]
#gtf_file = 'aspergillus_nidulans_used_proteins.gtf'
exon_df = pd.read_csv(exon_file,delimiter='\t',header=None).values
domain_df = pd.read_csv(domain_file,delimiter='\t',header=None).values

exon_tids = exon_df[:,0]
domain_tids = domain_df[:,0]


exons = {}
for i in range(exon_df.shape[0]):
    tid = exon_tids[i]
    if not (tid in exons):
        exons[tid] = []
    exons[tid].append(exon_df[i,1])

num_domain = 0
num_exon = 0
O = 0
boxes = {}
aligned_domains = []
proteins_used = []
for i in range(domain_df.shape[0]):
    tid = domain_tids[i]
    if not (tid in boxes):
        boxes[tid] = []
    supfamid = domain_df[i,1]
    start_box = domain_df[i,2].split('-')
    start_start = int(start_box[0])
    start_end = int(start_box[1])
    end_box = domain_df[i,3].split('-')
    end_start = int(end_box[0])
    end_end = int(end_box[1])
    boxes[tid].append(domain_df[i,2])
    boxes[tid].append(domain_df[i,3])
    in_start = False
    in_end = False
    if tid in exons:
        if not (tid in proteins_used):
            proteins_used.append(tid)
        num_domain += 1
        for exon in exons[tid]:
            if '-' in exon:
                exon_l = int(exon.split('-')[0])
                exon_r = exon_l+1
                if (((exon_l >= start_start) and (exon_l) <= start_end) or ((exon_r >= start_start) and (exon_r) <= start_end)):
                    in_start = True
                if (((exon_l >= end_start) and (exon_l) <= end_end) or ((exon_r >= end_start) and (exon_r) <= end_end)):
                    in_end = True
            else:
                exon = int(exon)
                if ((exon >= start_start) and (exon <= start_end)):
                    in_start = True
                if ((exon >= end_start) and (exon <= end_end)):
                    in_end = True
        if in_start or in_end:
            aligned_domains.append(supfamid)




print('number of proteins used: %d' % len(proteins_used))
print('number of domains used %d' % num_domain)
print('O_domain: %d' % len(aligned_domains))
