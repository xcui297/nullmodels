import numpy as np
import sys
import pandas as pd

domain_boxes_file = sys.argv[1]
out_file = sys.argv[2]

domain_boxes_data = pd.read_csv(domain_boxes_file,delimiter='\t',header=None).values
mapping = {}
for i in range(domain_boxes_data.shape[0]):
    domain = domain_boxes_data[i,1]
    protein = domain_boxes_data[i,0]
    if domain in mapping:
        if not (protein in mapping[domain]):
            mapping[domain].append(protein)
    else:
        mapping[domain] = [protein]

with open(out_file,'w') as f:
    for domain in mapping:
        f.write("%d" % domain)
        for protein in mapping[domain]:
            f.write("\t%s" % protein)
        f.write("\n")
